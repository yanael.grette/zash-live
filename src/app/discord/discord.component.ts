import { Component, OnInit } from '@angular/core';
import { images_urls, urls } from '../utils/path-values';


@Component({
  selector: 'app-discord',
  templateUrl: './discord.component.html',
})


export class DiscordComponent implements OnInit {

  imgPath : string;
  linkPath: string;
  constructor() { }

  ngOnInit() {
    this.imgPath =  images_urls.discord_image;
    this.linkPath = urls.discord_link;
  }

}
