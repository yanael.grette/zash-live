import { Component, OnInit } from '@angular/core';
import { images_urls,urls } from '../utils/path-values';

@Component({
  selector: 'app-twitter',
  templateUrl: './twitter.component.html'
})
export class TwitterComponent implements OnInit {
  imgPath  :  string;
  linkPath : string;
  constructor() { }

  ngOnInit() {
    this.imgPath = images_urls.twitter_image;
    this.linkPath = urls.twitter_link;
  }

}
