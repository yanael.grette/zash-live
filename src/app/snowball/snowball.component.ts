import { Component, OnInit } from '@angular/core';
import { images_urls, urls } from '../utils/path-values';

@Component({
  selector: 'app-snowball',
  templateUrl: './snowball.component.html'
})
export class SnowballComponent implements OnInit {

  imgPath : string;
  linkPath : string;
  constructor() { }

  ngOnInit() {
    this.imgPath = images_urls.snowball_image;
    this.linkPath = urls.snowball_link;
  }

}
