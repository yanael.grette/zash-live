import { Component, OnInit } from '@angular/core';


import { images_urls, urls} from '../utils/path-values';

@Component({
  selector: 'app-twitch',
  templateUrl: './twitch.component.html'
})
export class TwitchComponent implements OnInit {
  imgPath : string;
  linkPath : string;


  ngOnInit() {
    this.imgPath = images_urls.twitch_image;
    this.linkPath = urls.twitch_link;
  }
}
