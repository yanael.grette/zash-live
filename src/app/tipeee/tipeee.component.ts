import { Component, OnInit } from '@angular/core';
import { images_urls, urls } from '../utils/path-values';

@Component({
  selector: 'app-tipeee',
  templateUrl: './tipeee.component.html'
})
export class TipeeeComponent implements OnInit {
  imgPath : string;
  linkPath : string;
  constructor() { }

  ngOnInit() {
    this.imgPath = images_urls.tipeee_image;
    this.linkPath = urls.tipeee_link;
  }

}
