import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatExpansionModule} from '@angular/material/expansion';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import { AppComponent } from './app.component';
import { DiscordComponent } from './discord/discord.component';
import { InstagramComponent } from './instagram/instagram.component';
import { SkillbookComponent } from './skillbook/skillbook.component';
import { SnowballComponent } from './snowball/snowball.component';
import { TipeeeComponent } from './tipeee/tipeee.component';
import { TwitchComponent } from './twitch/twitch.component';
import { TwitterComponent } from './twitter/twitter.component';
import { UtipComponent } from './utip/utip.component';
import { YoutubeComponent } from './youtube/youtube.component';


@NgModule({
  declarations: [
    AppComponent,
    DiscordComponent,
    InstagramComponent,
    SkillbookComponent,
    SnowballComponent,
    TipeeeComponent,
    TwitchComponent,
    TwitterComponent,
    UtipComponent,
    YoutubeComponent
  ],
  imports: [
    BrowserModule,
    MatSlideToggleModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MatExpansionModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
