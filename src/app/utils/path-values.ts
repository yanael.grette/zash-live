const assets_url = "../assets/"

//url image
const base_image_url = assets_url+"logo/";
const discord_image = base_image_url+"discord.png";
const instagram_image = base_image_url+"instagram.png";
const skillbook_image = base_image_url+"skillbook.png";
const snowball_image = base_image_url+"snowball.png";
const tipeee_image = base_image_url+"tipeee.png";
const twitch_image = base_image_url+"twitch.png";
const twitter_image = base_image_url+"twitter.png";
const utip_image = base_image_url+"utip.png";
const youtube_image = base_image_url+"youtube.png";

const images_urls = {
  discord_image,
  instagram_image,
  skillbook_image,
  snowball_image,
  tipeee_image,
  twitch_image,
  twitter_image,
  utip_image,
  youtube_image
};

export  { images_urls };

const base_icon_url = assets_url+"icons/";
const on_icon = base_icon_url+"on.ico";
const off_icon = base_icon_url+"off.ico";
const icon_twitch = base_icon_url+"twitch_profile.png";

const icons_urls = {
  icon_twitch,
  on_icon,
  off_icon
}

export { icons_urls };


//url
const discord_link = "https://discord.com/invite/nvhkzje";
const instagram_link = "https://www.instagram.com/eos_zash/";
const skillbook_link = "https://zash.teachable.com/p/zash-lol-skillbook";
const snowball_link = "https://snowball.gg/fr/league-of-legends/coach/zash";
const tipeee_link = "https://fr.tipeee.com/eos_zash";
const twitch_link = "https://twitch.tv/eos_zash";
const twitter_link = "https://twitter.com/eos_zash";
const utip_link = "https://utip.io/zash";
const youtube_link = "https://www.youtube.com/user/EGGZash";

const urls = {
  discord_link,
  instagram_link,
  skillbook_link,
  snowball_link,
  tipeee_link,
  twitch_link,
  twitter_link,
  utip_link,
  youtube_link
};

export {urls};
