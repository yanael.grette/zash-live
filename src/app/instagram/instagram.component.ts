import { Component, OnInit } from '@angular/core';
import { images_urls, urls } from '../utils/path-values';

@Component({
  selector: 'app-instagram',
  templateUrl: './instagram.component.html'
})
export class InstagramComponent implements OnInit {

  imgPath : string;
  linkPath :  string;

  constructor() { }

  ngOnInit() {
    this.imgPath = images_urls.instagram_image;
    this.linkPath = urls.instagram_link;
  }

}
