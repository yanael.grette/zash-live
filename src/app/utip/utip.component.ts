import { Component, OnInit } from '@angular/core';
import { images_urls, urls } from '../utils/path-values';

@Component({
  selector: 'app-utip',
  templateUrl: './utip.component.html'
})
export class UtipComponent implements OnInit {

  imgPath : string;
  linkPath :  string;

  constructor() { }

  ngOnInit() {
    this.imgPath = images_urls.utip_image;
    this.linkPath = urls.utip_link;
  }

}
