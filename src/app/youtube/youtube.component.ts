import { Component, OnInit } from '@angular/core';
import { images_urls, urls } from '../utils/path-values';


@Component({
  selector: 'app-youtube',
  templateUrl: './youtube.component.html'
})
export class YoutubeComponent implements OnInit {
  imgPath :  string;
  linkPath : string;
  constructor() { }

  ngOnInit() {
    this.imgPath = images_urls.youtube_image;
    this.linkPath = urls.youtube_link;
  }

}
