import { Component, ViewEncapsulation, OnInit } from '@angular/core';

import { environment } from "../environments/environment";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls : ['./app.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
  title = 'Zash-Live';
  son : boolean;
  notification : boolean;

  ngOnInit() {
    this.notification = JSON.parse(localStorage.getItem(environment.storage.notifKey));
    this.son = JSON.parse(localStorage.getItem(environment.storage.soundNotifKey));
  }

  switchNotif() {
    if(!this.notification) {
      this.son = false;
      localStorage.setItem(environment.storage.soundNotifKey, JSON.stringify(this.son));
    }
    localStorage.setItem(environment.storage.notifKey, JSON.stringify(this.notification));
  }
  switchSon() {
    if(this.son) {
      this.notification = true;
      localStorage.setItem(environment.storage.notifKey, JSON.stringify(this.notification));
    }
    localStorage.setItem(environment.storage.soundNotifKey, JSON.stringify(this.son));
  }
}
