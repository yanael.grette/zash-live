import { Component, OnInit } from '@angular/core';
import { images_urls, urls } from '../utils/path-values';

@Component({
  selector: 'app-skillbook',
  templateUrl: './skillbook.component.html'
})
export class SkillbookComponent implements OnInit {
  imgPath : string;
  linkPath : string;
  constructor() { }

  ngOnInit() {
    this.imgPath = images_urls.skillbook_image;
    this.linkPath = urls.skillbook_link;
  }

}
