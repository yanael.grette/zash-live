import { environment } from "../environments/environment";

import { urls } from "../app/utils/path-values";

import { PushNotificationsService} from 'ng-push';


export class Notification {

  private notifications : PushNotificationsService;
  constructor() {
    this.notifications = new PushNotificationsService();
    this.notifications.requestPermission();
  }
  notifier(icon, titre, texte) {
    let notification = JSON.parse(localStorage.getItem(environment.storage.notifKey));
    if(notification) {
      let son = JSON.parse(localStorage.getItem(environment.storage.soundNotifKey));

      let options = {
        body: texte,
        icon: icon,
        silent : !son
      }
      this.notifications.create(titre, options).subscribe(
        res => {
          if (res.event.type === 'click') {
            chrome.tabs.create({url :   urls.twitch_link});
            res.notification.close();
          }
        }
      );
    }
  }
}
