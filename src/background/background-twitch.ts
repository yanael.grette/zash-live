import axios from "axios";
import { AxiosInstance } from "axios";

import { Notification } from "./notification";

import { icons_urls } from "../app/utils/path-values";
import { environment } from "../environments/environment";


chrome.browserAction.setIcon({
  path : icons_urls.off_icon
});
var currentToken : string = "";
var timeout : number = 5000;
var infoLive :  {};
var axiosToken : AxiosInstance;
var axiosStream : AxiosInstance;
var axiosValidate : AxiosInstance;
var enLive = true;
var notification = new Notification();


let localToken = localStorage.getItem(environment.storage.tokenKey);
currentToken = localToken ? localToken : "_";

axiosToken = axios.create( {
  baseURL : environment.twitch_details.complete_token_url
});




function start() {
  setAxios();
  getEtatLive();
}

function setAxios() {
  axiosValidate = axios.create( {
    baseURL : environment.twitch_details.validate_url,
    headers : { "Authorization": "OAuth "+currentToken}
  });


  axiosStream = axios.create( {
    baseURL : environment.twitch_details.complete_stream_url,
    headers :  {
      "Authorization": "Bearer "+currentToken,
      "Client-ID": environment.twitch_details.client_id
    }
  });
}
function checkToken() {
  axiosValidate.get("")
  .then( function(response) {
    getEtatLive();
  })
  .catch(function (error) {
    setToken();
  });
}

function setToken() {
  axiosToken.post("")
  .then( function(response) {
    currentToken = response.data["access_token"];
    setAxios();
    localStorage.setItem(environment.storage.tokenKey, currentToken);
    checkToken();
  })
  .catch(function (error) {
    setToken();
  });
}

function getEtatLive() {
  axiosStream.get("")
  .then( function(response) {
    setEtatLive(response["data"]["data"]);
  })
  .catch(function (error) {
    setToken();
  });
}

function setEtatLive(data) {
  let icon = icons_urls.on_icon;
  infoLive = data[0];
  if(infoLive) {
    if(!enLive) {
      enLive = true;
      console.log("LE MEILLEUR  LANCE ENCORE UN INCROYABLE STREAM !!! LET'S GO !")
      notification.notifier(
        icons_urls.icon_twitch,
        "Le stream est lancé !",
        infoLive["title"]);
    }
  }
  else {
    enLive = false;
    icon = icons_urls.off_icon;
  }
  chrome.browserAction.setIcon({
    path : icon
  });
  setTimeout(() => getEtatLive(), timeout);
}

console.log("LEEEEEEEEEET'S GO ZASH ! LET'S GO !!!")
start();
