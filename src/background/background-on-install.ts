import { environment } from "../environments/environment";
chrome.runtime.onInstalled.addListener(() => {
  let val = true;
  localStorage.setItem(environment.storage.notifKey, JSON.stringify(val));
  localStorage.setItem(environment.storage.soundNotifKey, JSON.stringify(val));
});
