const validate_url = "https://id.twitch.tv/oauth2/validate"
const token_url = "https://id.twitch.tv/oauth2/token";
const stream_url = "https://api.twitch.tv/helix/streams";
const client_id = "il2am5q1frehqdszlgwodzei6793bi";
const client_id_attribute = "client_id";
const client_secret = "4piglvo7byw4qqfkbogfgs6glqcjtw";
const client_secret_attribute = "client_secret";
const grant_type_token = "client_credentials";
const grant_type_refresh = "refresh_token";
const grant_type_attribute = "grant_type";
const streamer = "eos_zash";
const user_attribute = "user_login";
const complete_token_url = token_url+"?"+client_id_attribute+"="+client_id+"&"+client_secret_attribute+"="+client_secret+"&"+grant_type_attribute+"="+grant_type_token;
const complete_stream_url = stream_url+"?"+user_attribute+"="+streamer;


const twitch_details = {
  complete_token_url:complete_token_url,
  complete_stream_url:complete_stream_url,
  client_id:client_id,
  validate_url : validate_url
};

const tokenKey = "Zash-Live-Token-browser-extension";
const notifKey = "Zash-Live-Notif-browser-extension";
const soundNotifKey = "Zash-Live-SoundNotif-browser-extension";

const storage = {
  tokenKey,
  notifKey,
  soundNotifKey
};


export const environment = {
  production: true,
  twitch_details,
  storage
};
