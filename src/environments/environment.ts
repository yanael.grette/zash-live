// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

const validate_url = "https://id.twitch.tv/oauth2/validate"
const token_url = "https://id.twitch.tv/oauth2/token";
const stream_url = "https://api.twitch.tv/helix/streams";
const client_id = "il2am5q1frehqdszlgwodzei6793bi";
const client_id_attribute = "client_id";
const client_secret = "4piglvo7byw4qqfkbogfgs6glqcjtw";
const client_secret_attribute = "client_secret";
const grant_type_token = "client_credentials";
const grant_type_refresh = "refresh_token";
const grant_type_attribute = "grant_type";
const streamer = "eos_zash";
const user_attribute = "user_login";
const complete_token_url = token_url+"?"+client_id_attribute+"="+client_id+"&"+client_secret_attribute+"="+client_secret+"&"+grant_type_attribute+"="+grant_type_token;
const complete_stream_url = stream_url+"?"+user_attribute+"="+streamer;


const twitch_details = {
  complete_token_url:complete_token_url,
  complete_stream_url:complete_stream_url,
  client_id:client_id,
  validate_url : validate_url
};

const tokenKey = "Zash-Live-Token-browser-extension";
const notifKey = "Zash-Live-Notif-browser-extension";
const soundNotifKey = "Zash-Live-SoundNotif-browser-extension";

const storage = {
  tokenKey,
  notifKey,
  soundNotifKey
};

export const environment = {
  production: false,
  twitch_details,
  storage
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error',  // Included with Angular CLI.
